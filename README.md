
## Starting template for static sites

#### Quickstart:

    npm install
    gulp serve

#### Tools (DEVELOPMENT):

- node.js
- gulp
- sass
- es6

Before production, make sure to build files using `gulp build` command.

#### Production Requirements:

- php (>5.4)

---
