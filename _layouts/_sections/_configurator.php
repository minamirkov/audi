<section class="m-configurator " id="configurator">
    <h4> You might take a look... </h4>
    <ul class="m-configurator__links">
        <li><a href="#one" class="-active">All </a></li>
        <li><a href="#two">Hatchback </a></li>
        <li><a href="#three">Sportback </a></li>
        <li><a href="#four">Limousine </a></li>
        <li><a href="#five">SUV </a></li>
        <li><a href="#six">Cabrio </a></li>
    </ul>
    <div class="_wr">
        <div class="_w m-configurator__content -active" id="one">
            <!--first-->
            <div class="_12 _s6 _xl3 -first" id="js-FirstConfiguratorLink">
                <article class="m-configurator__content--box">
                    <div class="-image">
                        <img src="src/images/audi_q3_ant.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi Q3 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price -prices" id="js-price" data-price="29999" data-name="one">  29.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>

                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                </article>
            </div>
            <!--            second-->
            <div class="_12 _s6 _xl3 -second">
                <article class="m-configurator__content--box">
                    <div class="-image">
                        <img src="src/images/audi_a4.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi A4 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price -prices" data-price="24999" data-name="two">  24.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>
                            <!--                            <span class="-price -prices" data-price="24999" data-name="two">  24.999 </span>-->
                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                </article>
            </div>
            <!--            third-->
            <div class="_12 _s6 _xl3 -third">
                <article class="m-configurator__content--box">

                    <div class="-image">
                        <img src="src/images/audi_a3.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi A3 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price -prices" data-price="19999" data-name="three">  19.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>
                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                    <span class="-new"> NEW </span>
                </article>
            </div>
            <!--            fourth-->
            <div class="_12 _s6 _xl3 -fourth">
                <article class="m-configurator__content--box">

                    <div class="-image">
                        <img src="src/images/audi_q7.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi Q7 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price -prices" data-price="39999" data-name="four">  39.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>

                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                    <span class="-new"> NEW </span>
                </article>
            </div>
        </div>

        <div class="_w m-configurator__content" id="two">
            <!--first-->
            <div class="_12 _s6 _xl3 -first">
                <article class="m-configurator__content--box ">
                    <div class="-image">
                        <img src="src/images/audi_q3_ant.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi Q3 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price" data-price="29999" data-name="one">  29.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>

                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                </article>
            </div>
            <!--            second-->
            <div class="_12 _s6 _xl3 -second">
                <article class="m-configurator__content--box">
                    <div class="-image">
                        <img src="src/images/audi_a4.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi A4 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price" data-price="24999" data-name="two">  24.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>
                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                </article>
            </div>
        </div>
        <div class="_w m-configurator__content" id="three">
            <!--first-->
            <div class="_12 _s6 _xl3 -first">
                <article class="m-configurator__content--box ">
                    <div class="-image">
                        <img src="src/images/audi_q3_ant.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi Q3 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price" data-price="29999" data-name="one">  29.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>

                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                </article>
            </div>
            <!--            third-->
            <div class="_12 _s6 _xl3 -third">
                <article class="m-configurator__content--box">

                    <div class="-image">
                        <img src="src/images/audi_a3.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi A3 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price" data-price="19999" data-name="three">  19.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>

                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                    <span class="-new"> NEW </span>
                </article>
            </div>
        </div>
        <div class="_w m-configurator__content" id="four">
            <!--first-->
            <div class="_12 _s6 _xl3 -first">
                <article class="m-configurator__content--box">

                    <div class="-image">
                        <img src="src/images/audi_a3.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi A3 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price" data-price="19999" data-name="one">  19.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>

                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                    <span class="-new"> NEW </span>
                </article>
            </div>
        </div>
        <div class="_w m-configurator__content">
            <!--            third-->
            <div class="_12 _s6 _xl3 -third">
                <article class="m-configurator__content--box">

                    <div class="-image">
                        <img src="src/images/audi_a3.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi A3 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>
                            <span class="-price" data-price="19999" data-name="three">  19.999 </span>
                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                    <span class="-new"> NEW </span>
                </article>
            </div>
            <!--            fourth-->
            <div class="_12 _s6 _xl3 -fourth">
                <article class="m-configurator__content--box">

                    <div class="-image">
                        <img src="src/images/audi_q7.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi Q7 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price" data-price="39999" data-name="four">  39.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>

                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                    <span class="-new"> NEW </span>
                </article>
            </div>
        </div>
        <div class="_w m-configurator__content" id="five">
            <!--            third-->
            <div class="_12 _s6 _xl3">
                <article class="m-configurator__content--box">

                    <div class="-image">
                        <img src="src/images/audi_a3.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi A3 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price" data-price="19999" data-name="three">  19.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>

                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                    <span class="-new"> NEW </span>
                </article>
            </div>
            <!--            fourth-->
            <div class="_12 _s6 _xl3 -fourth">
                <article class="m-configurator__content--box">

                    <div class="-image">
                        <img src="src/images/audi_q7.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi Q7 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price" data-price="39999" data-name="four">  39.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>

                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                    <span class="-new"> NEW </span>
                </article>
            </div>
        </div>
        <div class="_w m-configurator__content" id="six">
            <!--            fourth-->
            <div class="_12 _s6 _xl3 -fourth">
                <article class="m-configurator__content--box">

                    <div class="-image">
                        <img src="src/images/audi_q7.png" alt="">
                    </div>
                    <span> 5 Seats, 4 Doors </span>
                    <span class="-heading"> Audi Q7 2020 </span>
                    <a class="-blue"> Configurator </a>
                    <div class="-checked">
                        <span> Includes 300 free km </span>
                        <span> Theft Protection </span>
                        <span> Non Refundable </span>
                    </div>
                    <span class="-price" data-price="39999" data-name="four">  39.999 </span>
                    <div class="-pricePart">
                        <div class="-priceTag">
                            <span class="-starting"> Starting from </span>
                        </div>
                        <div>
                            <button> Buy Now</button>
                        </div>
                    </div>
                    <span class="-new"> NEW </span>
                </article>
            </div>
        </div>
    </div>
</section>