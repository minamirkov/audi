document.addEventListener('DOMContentLoaded', function() {
    const menuItems = document.querySelectorAll('.m-nav__menu--items');
    const hamburger = document.querySelector('.m-nav__button');
    const overlay = document.querySelector('.js-navOverlay');
    const _body = document.querySelector('body');

    function toggleMenu() {
        _body.classList.toggle('-open');
    }

    hamburger.addEventListener('click', toggleMenu);

    overlay.addEventListener('click', toggleMenu);

    menuItems.forEach(function (menuItem) {
        menuItem.addEventListener('click', toggleMenu);
    });

    const swiper = new Swiper('.mySwiper', {
        // loop: true,

        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    var element = document.querySelector('.m-nav__menu--content');
    var navigation = document.querySelector('.m-nav');

    function Scroll (e) {
        e.preventDefault();
        var ypos = window.pageYOffset;

        if (ypos > 400) {
            element.classList.add('-scroll');
        }
        else {
            element.classList.remove('-scroll');
        }
    }

    function Scroll2 () {
        var ypos = window.pageYOffset;

        if (ypos > 100) {
            navigation.classList.add('-scroll');
        }
        else {
            navigation.classList.remove('-scroll');
        }
    }

    window.addEventListener('scroll', Scroll);

    window.onscroll = Scroll2;

    const links = document.querySelectorAll('.m-nav__menu a');

    for (const link of links) {
        link.addEventListener('click', clickHandler);
    }

    function clickHandler(e) {
        e.preventDefault();
        const href = this.getAttribute('href');
        const offsetTop = document.querySelector(href).offsetTop;

        scroll({
            top: offsetTop,
            behavior: 'smooth'
        });
    }
});