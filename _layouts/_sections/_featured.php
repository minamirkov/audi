<section class="m-featured"
         style="background-image: linear-gradient(270deg, rgba(0,0,0,0) 0%, #000 100%), url('src/images/featured-banner.jpg') ">
    <div class="_wr">
        <div class="_w">
            <div class="_12 _m6">
                <span class="a-span"> Featured</span>
                <h3> Brand new The Audi RSQ8 </h3>
                <p> The RS Q8 gives you the choice: depending on the selected equipment, its looks are even bolder and
                    sportier to suit your personal ambitions.
                </p>
            </div>
        </div>
        <div class="m-featured__buttons _w">
            <div class="_12 _s8 _m3 _l2">
                <div class="m-featured__buttons--details">
                    <span> Power Output </span>
                    <span> 441 kw</span>
                    <span> (600 hp) </span>
                </div>
            </div>
            <div class="_12 _s8 _m3 _l2">
                <div class="m-featured__buttons--details">
                    <span> Acceleration </span>
                    <span> 3.8 </span>
                    <span> seconds </span>
                </div>
            </div>

            <div class="_12 _s8 _m3 _l2">
                <div class="m-featured__buttons--details">
                    <span> Torque </span>
                    <span> 800 </span>
                    <span> Nm </span>
                </div>
            </div class="_2">
        </div>
    </div>
</section>