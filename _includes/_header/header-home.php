<header class="m-header" id="home">
    <div class="m-overlay js-navOverlay"></div>
    <nav class="m-nav">
        <a href=""><img class="m-nav__logo" src="src/images/audi_logo.png" alt=""> </a>
        <div class="m-nav__menu">
            <a href="#home" class="m-nav__menu--items"> Homepage </a>
            <div class="m-nav__menu--dropdown">
                <a href="#about" class="m-nav__menu--items"> About us </a>
                <div class="m-nav__menu--content">
                    <a href="#"> Our Team </a>
                    <a href="#"> Leadership </a>
                    <a href="#"> Partners </a>
                </div>
            </div>
            <a href="#configurator" class="m-nav__menu--items"> Products </a>
            <a href="#contact" class="a-button -contact"> Contact us </a>
        </div>
        <button class="m-nav__button js-toggleNav">
            <span></span>
            <span></span>
            <span></span>
        </button>
    </nav>
    <section class="m-hero">
        <div class="swiper mySwiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide" style="background-image: url('src/images/header-img.jpg')">
                    <div class="_wr">
                        <div class="_w">
                            <section class="m-hero__text ofs_1 _11 ofs_l0 _l5">
                                <span class="a-span"> new arrival </span>
                                <h1> Expectations put limitations on possibilities. </h1>
                                <div class="m-hero__text--paragraph">
                                    <p> The Audi Q8 offers a unique performance that blends sport and utility into a
                                        momentous SUV
                                        design. </p>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide" style="background-image: url('src/images/header-img.jpg')">
                    <div class="_wr">
                        <div class="_w">
                            <section class="m-hero__text ofs_1 _11 ofs_l0 _l5">
                                <span class="a-span"> new arrival </span>
                                <h1> Expectations put limitations on possibilities. </h1>
                                <div class="m-hero__text--paragraph">
                                    <p> The Audi Q8 offers a unique performance that blends sport and utility into a
                                        momentous SUV
                                        design. </p>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide" style="background-image: url('src/images/header-img.jpg')">
                    <div class="_wr">
                        <div class="_w">
                            <section class="m-hero__text ofs_1 _11 ofs_l0 _l5">
                                <span class="a-span"> new arrival </span>
                                <h1> Expectations put limitations on possibilities. </h1>
                                <div class="m-hero__text--paragraph">
                                    <p> The Audi Q8 offers a unique performance that blends sport and utility into a
                                        momentous SUV
                                        design. </p>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </section>
</header>