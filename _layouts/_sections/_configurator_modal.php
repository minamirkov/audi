<div class="m-modal__overlay js-modalOverlay"></div>
<section class="m-modal">
    <div class="_wr">
        <div class="_w" id="js-firstConfigurator">
            <div class="m-modal__right _12 ofs_l3 _l6 ">
                <span> Audi Configurator</span>
                <span> Create your own audi.</span>
                <form action="" class="-form" name="listForm">
                    <div class="m-modal__right--flex">
                        <div>
                            <input type="checkbox" id="esystem" value="0" class="checkbox">
                            <label for="esystem"> 48-V electrical system </label>
                        </div>
                        <span> 0, </span>
                    </div>

                    <div>
                        <div>
                            <input type="checkbox" id="suspension" value="278.60" class="checkbox">
                            <label for="suspension"> Adaptive air suspension - sport </label>
                        </div>
                        <span> 278,60 </span>
                    </div>

                    <div>
                        <div>
                            <input type="checkbox" id="windows" value="558.78" class="checkbox">
                            <label for="windows"> Acoustic side windows - optional </label>
                        </div>
                        <span> 558,78 </span>
                    </div>

                    <div>
                        <div>
                            <input type="checkbox" id="glass" value="357.29" class="checkbox">
                            <label for="glass"> Acoustic and heat-insulating glass </label>
                        </div>
                        <span> 357,29 </span>
                    </div>

                    <div>
                        <div>
                            <input type="checkbox" id="led" value="637.48" class="checkbox">
                            <label for="led"> Ambiental LED </label>
                        </div>
                        <span> 637,48 </span>
                    </div>

                </form>

            </div>
        </div>
        <div class="m-modal__span js-toggleModal js-closeModal">
            <span></span>
            <span></span>
            <span></span>
        </div>
</section>
