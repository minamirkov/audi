<footer class="m-footer" id="contact">
    <div class="_wr">
        <div class="_w">
            <div class="m-footer__text _12 _s10 _l6">
                <span class="a-span"> Contact us </span>
                <h5> If you have any questions, don’t hesitate to contact us. </h5>
            </div>
            <img src="src/images/contact-form-img.png" alt="">
            <div class="_s12 ofs_l1 _l5">
                <form class="m-footer__form" action="">
                    <div class="form-group">
                        <label for="name"> Enter your Name <sup class="a-span"> * </sup> </label>
                        <input type="text" required>
                    </div>


                    <div class="form-group">
                        <label for="email"> Enter your Email <sup class="a-span"> * </sup> </label>
                        <input type="email" required>
                    </div>
                    <div class="form-group">
                        <label for="textarea"> Your Message <sup class="a-span"> * </sup> </label>
                        <textarea name="" id="" cols="10" rows="5" required></textarea>
                    </div>

                    <div class="-submit">
                        <input type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="m-footer__links">
        <div>
            <span> © 2020 FSD. All Rights Reserved </span>
        </div>
        <div>
            <a href=""> Terms of agreement </a>
            <a href=""> Privacy Policy </a>
        </div>
    </div>

</footer>

<?php require_once(dirname(__FILE__) . '/footer_meta.php'); ?>