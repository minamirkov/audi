<section class="proba">

    <div class="div-links">
        <button data-value="all" class="links"> All items</button>
        <button data-id="section1" class="links"> Audi Q3</button>
        <button data-id="section2" class="links"> Audi A4</button>
        <button data-id="section3" class="links"> Audi A3</button>
        <button data-id="section4" class="links"> Audi Q7</button>
    </div>

    <div class="_wr">
        <div class="_w">
            <!--first-->
            <div class="_12 _m6 _l3 box" data-id="section1">
                <div>
                    <img src="src/images/audi_q3_ant.png" alt="">
                </div>
                <span> 5 Seats, 4 Doors </span>
                <span> Audi Q3 2020 </span>
                <a class="button"> Configurator </a>
                <div>
                    <span> Includes 300 free km </span>
                    <span> Theft Protection </span>
                    <span> Non Refundable </span>
                </div>
                <span>  29.999 </span>
                <div>
                    <div>
                        <span> Starting from </span>

                    </div>
                    <div>
                        <button class="-red"> Buy Now</button>
                    </div>
                </div>

            </div>
            <!-- second-->
            <div class="_12 _m6 _l3 box" data-id="section2">

                <div>
                    <img src="src/images/audi_a4.png" alt="">
                </div>
                <span> 5 Seats, 4 Doors </span>
                <span> Audi A4 2020 </span>
                <a class="button"> Configurator </a>
                <div>
                    <span> Includes 300 free km </span>
                    <span> Theft Protection </span>
                    <span> Non Refundable </span>
                </div>
                <span>  24.999 </span>
                <div>
                    <div>
                        <span> Starting from </span>
                    </div>
                    <div>
                        <button class="-red"> Buy Now</button>
                    </div>
                </div>

            </div>
            <!-- third-->
            <div class="_12 _m6 _l3 box" data-id="section3">
                <div>
                    <img src="src/images/audi_a3.png" alt="">
                </div>
                <span> 5 Seats, 4 Doors </span>
                <span> Audi A3 2020 </span>
                <a class="button"> Configurator </a>
                <div>
                    <span> Includes 300 free km </span>
                    <span> Theft Protection </span>
                    <span> Non Refundable </span>
                </div>
                <span>  19.999 </span>
                <div>
                    <div>
                        <span> Starting from </span>
                    </div>
                    <div>
                        <button class="-red"> Buy Now</button>
                    </div>
                </div>
                <span class="-new"> NEW </span>
            </div>
            <!-- fourth-->

            <article class="_12 _m6 _l3 box" data-id="section4">
                <div>
                    <img src="src/images/audi_q7.png" alt="">
                </div>
                <span> 5 Seats, 4 Doors </span>
                <span> Audi Q7 2020 </span>
                <a class="button"> Configurator </a>
                <div>
                    <span> Includes 300 free km </span>
                    <span> Theft Protection </span>
                    <span> Non Refundable </span>
                </div>
                <span>  39.999 </span>
                <div>
                    <div>
                        <span> Starting from </span>

                    </div>
                    <div>
                        <button class="-red"> Buy Now</button>
                    </div>
                </div>
                <span class="-new"> NEW </span>

            </article>
        </div>
    </div>
</section>