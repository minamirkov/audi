<section class="m-about" id="about">
    <div class="_wr">
        <div class="_w">
            <div class="m-about__left ofs_0 _12 ofs_s3 _s6 _l4 ofs_l1">
                <img src="src/images/audiq8.png" alt="">
                <p> A progressive design, a powerful engine. </p>

            </div>
            <div class="m-about__right _12 ofs_l2 _l5">
                <span class="a-span"> about q8</span>
                <h2> Welcome to the 8th dimension. The new Audi Q8 </h2>
                <p class="m-about__right--firstP"> With its imposing Singleframe in octagonal design, the Audi Q8 presents the new face of the Q
                    family. </p>
                <p class="m-about__right--secondP"> The radiator grille combines with the forward-mounted spoiler and the strongly contoured air inlets
                    to underline its confident look. </p>

            </div>
        </div>
    </div>

</section>