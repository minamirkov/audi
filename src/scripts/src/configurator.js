document.addEventListener('DOMContentLoaded', function () {
    const links = document.querySelectorAll('.m-configurator__links li a');

    for (let i = 0; i < links.length; i++) {
        links[i].addEventListener('click', function (e) {
            e.preventDefault();
            const activLinks = document.querySelector('.m-configurator__links li a.-active');
            const activContent = document.querySelector('.m-configurator__content.-active');

            activLinks.classList.remove('-active');
            activContent.classList.remove('-active');

            this.classList.add('-active');
            const attr = this.getAttribute('href');

            const activ = document.querySelector(attr);

            activ.classList.add('-active');

        });
    }


    const buttons = document.querySelectorAll('.-blue');
    const overlay = document.querySelector('.js-modalOverlay');
    const x = document.querySelector('.js-toggleModal');
    const _body = document.querySelector('body');

    function toggleMenu() {
        _body.classList.toggle('-openModal');
    }

    overlay.addEventListener('click', toggleMenu);

    x.addEventListener('click', toggleMenu);


    buttons.forEach(function (button) {
        const x = document.querySelector('.js-toggleModal');
        const overlay = document.querySelector('.js-modalOverlay');

        button.addEventListener('click', (e) => {
            toggleMenu();
            var show = e.target.closest('.m-configurator__content--box');
            console.log(show);
            show.classList.add('-show');

            function exitButton() {
                show.classList.remove('-show');

                const items = document.querySelectorAll('.checkbox');

                items.forEach((item) => {
                    item.checked = false;
                })
            }

            if (show.classList.contains('-show') === true) {
                function calculator() {
                    const items = document.querySelectorAll('.checkbox');

                    items.forEach((item) => {

                        item.addEventListener('change', function () {
                            var itemTotal = Number(show.children[5].getAttribute('data-price'));


                            items.forEach((item) => {
                                if (item.checked) {
                                    itemTotal += Number(item.value * 1);
                                }

                                var x = show.children[5];
                                x.innerHTML = String(parseInt(itemTotal)).substring(0, 2) + '.' + String(parseInt(itemTotal)).substring(2, itemTotal.length);
                            })
                        })
                    })

                }

                calculator();
            }


            x.addEventListener('click', exitButton);
            overlay.addEventListener('click', exitButton);
        });

    });

    // proba


    var linkButtons = document.querySelectorAll('.links');

    linkButtons.forEach(function (linkButton) {
        linkButton.addEventListener('click', function () {

            var linkValue = linkButton.getAttribute('data-id');
            var linkValueMain = linkButton.getAttribute('data-value');
            var boxes = document.querySelectorAll('.box');

            boxes.forEach(box => {
                var boxValue = box.getAttribute('data-id');

                if (linkValueMain === 'all' ) {
                    box.style.opacity = '1';


                } else if (boxValue === linkValue) {
                    box.style.opacity = '1';
                    box.children[3].addEventListener('click', function (){
                        console.log('radi')
                    });
                } else {
                    box.style.opacity = '0';
                }


            })
        })
    });
});